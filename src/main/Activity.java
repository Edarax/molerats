package main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Activity {
	/**
	 * Format of time 
	 */
	private static final SimpleDateFormat FORMAT = new SimpleDateFormat("HH:mm:ss");
	/**
	 * time in milisecondes that is used to separate activities 
	 */
	private static final long CALMTIME = 300000;
	/**
	 * minimum distance that this mole-rat walked
	 */
	private int walkedDistance;
	/**
	 * ID of last sensor that mole-rat triggered 
	 */
	private int lastStop;
	/**
	 * Time of last trigger of sensor 
	 */
	private Date timeOfLastStop;
	/**
	 * time molerat spend by briquette in milisecondes
	 */
	private long briquetteTime;
	/**
	 * length of activity in milisecondes
	 */
	private long activityLength;
	/*
	 * index of briquete sensor
	 */
	private static final int BRIQUETTESENSOR = 6;
	/**
	 * array of distances between 
	 */
	private static final int [][]DISTANCES = {{0, 0, 0, 0, 0, 0, 0},
											{0, 0, 34, 30, 92, 145, 141},
											{0, 34, 0, 54, 117, 113, 166},
											{0, 30, 54, 0, 64, 116, 113},
											{0, 92, 117, 64, 0, 64, 117},
											{0, 145, 113, 116, 64, 0, 54},
											{0, 141, 166, 113, 117, 54, 0},
											};
	
	/**
	 * @param lastStop - index of sensor that was pinged last time
	 * @param timeOfLastStop - time of last stop
	 */
	public Activity(int lastStop, String timeOfLastStop) {
		walkedDistance = 0;
		activityLength = 0;
		briquetteTime = 0;
		this.lastStop = lastStop;
		
		try {
			this.timeOfLastStop = FORMAT.parse(timeOfLastStop);
		} catch (ParseException e) {	
			e.printStackTrace();
		}
	}
	
	/**
	 * @param stop - id of stop antenna
	 * @param timeOfStop - time of receiving this signal
	 */
	public boolean addDistance(int stop, String timeOfStop, boolean overallActivty) {
		Date tmp = null;
		try {
			tmp = FORMAT.parse(timeOfStop);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		//briquette time
		if (lastStop == BRIQUETTESENSOR) {
			briquetteTime += tmp.getTime() - timeOfLastStop.getTime();
		}
		
		if (!overallActivty) {
			if (tmp.getTime() - timeOfLastStop.getTime() > CALMTIME) {
				return false;
			}
		}
		
		activityLength += tmp.getTime() - timeOfLastStop.getTime(); 
		timeOfLastStop = tmp;
		walkedDistance += DISTANCES[lastStop][stop];
		lastStop = stop;
		return true;
	}
	
	/**
	 * Predelat na {@code FORMAT} ? 
	 * @param milisecondes - that will be converted to time in "HH:mm:ss" format
	 * @return String in "HH:mm:ss" format
	 */
	private String milisedoncesToTime(long milisecondes){
		String output;
		long [] time = new long[3];
		time[0] = (long) milisecondes / 3600000;
	    time[2] = (long) milisecondes - time[0] * 3600000;
	    time[1] = time[2] / 60000;
	    time[2] -= time[1] * 60000;
	    time[2] /= 1000;
	    output = String.format("%02d:%02d:%02d", time[0], time[1], time[2]);
		return output;
	}
	
	/**
	 * @return string in "HH:mm:ss" format
	 */
	public String printTimeWithbriquette() {
		return milisedoncesToTime(briquetteTime);
	}

	public int getWalkedDistance() {
		return walkedDistance;
	}

	public void setWalkedDistance(int walkedDistance) {
		this.walkedDistance = walkedDistance;
	}

	public int getLastStop() {
		return lastStop;
	}

	public void setLastStop(int lastStop) {
		this.lastStop = lastStop;
	}

	@Override
	public String toString() {
		return "Minimum walked distance = " + walkedDistance + " cm, Activity durnation: " + milisedoncesToTime(activityLength);
	}

}	

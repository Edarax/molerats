package main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Karel Engl
 */
public class Main {	
	/**
	 * input reader
	 */
	private static Scanner SC = new Scanner(System.in);
	/**
	 * Splitter symbol in CSV file
	 */
	private static final String CSVSPLITBY = ";";
	/**
	 * position of time in CSV file
	 */
	private static final int TIMEPOSITION = 2;
	/**
	 * position of "Unit number" (antenna ID) in CSV file
	 */
	private static final int UNITNUMBERPOSITION = 3;
	/**
	 * position of ID of mole-rat in CSV file
	 */
	private static final int IDPOSITION = 6;
	
	/**
	 * @param args - input parameters
	 */
	public static void main(String[] args) {	
		//String FILENAME = "20181020.CSV";
		String FILENAME;
		
		System.out.println("Enter file name");
		FILENAME = SC.next();
		
		BufferedReader br = null;
        String line;     
        try {
        	br = new BufferedReader(new FileReader(FILENAME));
     	
        	ArrayList<Molerat> molerats = new ArrayList<Molerat>();      	
        	
        	//skipping first line
        	line = br.readLine();
        	
        	/**
        	 * reading of file and making results
        	 */
        	Molerat tmp;
        	boolean moleratFound;
           	
        	while ((line = br.readLine()) != null) {
	
                String[] row = line.split(CSVSPLITBY);
                int stop = Integer.parseInt(row[UNITNUMBERPOSITION]);
                moleratFound = false;   
                
                for (int i = 0; i < molerats.size(); i++) {
                	tmp = molerats.get(i);
                	if (tmp.getID().equals(row[IDPOSITION])) {
                		moleratFound = true;
                		tmp.addDistance(stop, row[TIMEPOSITION]);
                		break;
                	} 	
                }
                
                if (!moleratFound) {
                	molerats.add(new Molerat(row[IDPOSITION], stop, row[TIMEPOSITION]));    
                }
        	}
             	   	
        	/**
        	 * printing of results
        	 */	
        	PrintWriter pw = new PrintWriter("output.txt", "UTF-8");
        	for (int i = 0; i < molerats.size(); i++) {
        		System.out.println(molerats.get(i).toString());
        		pw.println(molerats.get(i).toString());
        	}
        	pw.close();       	
        } catch (FileNotFoundException e) {
        	e.printStackTrace();
        	System.out.println("Cannot find file: " + FILENAME);
        } catch (IOException e) {
        	e.printStackTrace();
        } finally {
        	if (br != null) {
        		try {
        			br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
		SC.next();
		SC.close();
	}
}

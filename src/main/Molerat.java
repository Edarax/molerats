package main;

import java.util.ArrayList;

/**
 * @author Karel Engl
 * this class is for storing data about specific mole-rat
 */
public class Molerat {
	/**
	 * ID of mole-rat
	 */
	private final String ID;
	/**
	 * all data about mole-rat in 1 activity
	 */
	private Activity overallActivity;
	/**
	 * separates activities
	 */
	private ArrayList<Activity> activities;
	
	/**
	 * @param ID - ID of mole-rat
	 * @param lastStop
	 * @param timeOfLastStop
	 */
	public Molerat(String ID, int lastStop, String timeOfLastStop) {
		this.ID = ID;
		activities = new ArrayList<Activity>();
		activities.add(new Activity(lastStop, timeOfLastStop));
		overallActivity = new Activity(lastStop, timeOfLastStop);
	}
	
	/**
	 * @param stop - id of stop antenna
	 * @param timeOfStop - time of receiving this signal
	 */
	public void addDistance(int stop, String timeOfStop) {
		overallActivity.addDistance(stop, timeOfStop, true);
		
		if (!activities.get(activities.size() - 1).addDistance(stop, timeOfStop, false)) {
			if (activities.get(activities.size() - 1).getWalkedDistance() == 0) {
				activities.remove(activities.size() - 1);			
			}
			activities.add(new Activity(stop, timeOfStop));
		}
	}

	@Override
	public String toString() {
		String output = "Molerat ID : " + ID + "\r\n"
				+ overallActivity.toString() + "\r\n";
		//redo in future
		output += "Time spend on briquette: " + overallActivity.printTimeWithbriquette() + "\r\n";
		
		for (int i = 0; i < activities.size(); i++) {
			output += "	Sub activity " + (i+1) + ": " + activities.get(i).toString() + "\r\n";
		}
		return output;
	}

	public String getID() {
		return ID;
	}
	
}
